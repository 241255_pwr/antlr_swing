package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;


public class MyTreeParser extends TreeParser {
	
	private LocalSymbols localSymbols = new LocalSymbols();


    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
		System.out.println(text.replace('\r', ' ').replace('\n', ' '));	
    	
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	

	protected void declareVar(String n) {
		localSymbols.newSymbol(n);
	}

	protected void setValue(String n, Integer v) {
		try {
			localSymbols.getSymbol(n);
		} catch (Exception e) {
			declareVar(n);
		}
		localSymbols.setSymbol(n, v);
	}

	protected Integer getValue(String n) {
		return localSymbols.getSymbol(n);
	}
	
	
	protected Integer modulo(Integer a, Integer b) {
		return a%b;
	}
	
	protected Integer power(Integer a, Integer b) {
	//		recursion
		   if (b != 0)
	            return (a * power(a, b - 1));
	        else
	            return 1;
	}
	
	protected Integer createBlock() {
		return localSymbols.enterScope();
	}
	
	protected Integer closeBlock() {
		return localSymbols.leaveScope();
	}
}
